use std::path::{Path, PathBuf};

#[derive(Debug)]
pub struct ChangedFiles {
    files: Vec<PathBuf>,
    repo_path: PathBuf,
}

impl ChangedFiles {
    pub fn new(repo_path: PathBuf) -> Self {
        Self {
            files: Vec::new(),
            repo_path,
        }
    }

    pub fn add<T>(&mut self, file: T)
    where
        T: Into<PathBuf>,
    {
        self.files.push(file.into())
    }

    pub fn iter_relative(&self) -> impl Iterator<Item = &Path> {
        let repo_path = self.repo_path.clone();
        self.files
            .iter()
            .map(move |file| file.strip_prefix(&repo_path).unwrap())
    }
}
