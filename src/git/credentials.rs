use std::path::PathBuf;

use anyhow::{bail, Context};
use git2::Cred;
use gix_url::{parse, Url};
use log::debug;

use crate::cli::Authentication;

#[derive(Clone)]
enum UsedAuthentication {
    None,
    Http {
        username: String,
        password: String,
    },
    Ssh {
        username: String,
        ssh_private_key: PathBuf,
        passphrase: Option<String>,
    },
}

#[derive(Clone)]
pub struct CredentialsResolver {
    used_authentication: UsedAuthentication,
}

impl CredentialsResolver {
    pub fn new(auth: Authentication) -> Self {
        let used_authentication = if let Some(http_password) = auth.http_password.clone() {
            debug!("Using http to authenticate with remote!");
            UsedAuthentication::Http {
                username: auth.auth_username,
                password: http_password,
            }
        } else if let Some(ssh_private_key) = auth.ssh_private_key.clone() {
            debug!("Using ssh to authenticate with remote!");
            UsedAuthentication::Ssh {
                username: auth.auth_username,
                ssh_private_key,
                passphrase: auth.ssh_passphrase.clone(),
            }
        } else {
            UsedAuthentication::None
        };
        CredentialsResolver {
            used_authentication,
        }
    }

    pub fn get_cred(&self) -> Result<Cred, git2::Error> {
        match &self.used_authentication {
            UsedAuthentication::Http { username, password, } => Cred::userpass_plaintext(username, password),
            UsedAuthentication::Ssh { username, ssh_private_key, passphrase, } => Cred::ssh_key(username, None, ssh_private_key, passphrase.as_ref().map(|p|p.as_str())),
            UsedAuthentication::None => Err(git2::Error::from_str("No valid authentication was provided! Either pass the http_password or ssh_private_key cli option!")),
        }
    }

    pub fn transform_remote_url(&self, remote_url: &str) -> anyhow::Result<String> {
        let original_url = parse(remote_url.into())
            .with_context(|| format!("Remote url {remote_url} is not a valid url!"))?;

        let (scheme, username, password) = match self.used_authentication.clone() {
            UsedAuthentication::Http { username, password } => (
                gix_url::Scheme::Https,
                Some(username),
                Some(password),
            ),
            UsedAuthentication::Ssh { username, .. } => (
                gix_url::Scheme::Ssh,
                Some(username),
                None,
            ),
            UsedAuthentication::None => bail!("Cannot transform remote url into authentication version as not authentication is specified!"),
        };
        let path = if original_url.path.starts_with(&[b'/']) {
            original_url.path.clone()
        } else {
            format!("/{}", original_url.path.clone()).into()
        };
        let adjusted_url = Url::from_parts(
            scheme,
            username,
            password,
            original_url.host().map(|s| s.into()),
            original_url.port,
            path,
            false,
        )?;

        debug!("Converted remote url to authenticated version:\n\told: {original_url}\n\tnew: {adjusted_url}");

        Ok(adjusted_url.to_string())
    }
}

impl From<Authentication> for CredentialsResolver {
    fn from(value: Authentication) -> Self {
        Self::new(value)
    }
}
