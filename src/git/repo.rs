use anyhow::Context;
use log::{debug, info, warn};
use std::{
    path::{Path, PathBuf},
    str::FromStr,
};
use thiserror::Error;

use git2::{
    build::CheckoutBuilder, Branch, BranchType, FetchOptions, Oid, PushOptions, Remote,
    RemoteCallbacks, Repository, Signature,
};

use crate::changed_files::ChangedFiles;

use super::credentials::CredentialsResolver;

const BASE_HEADS_REF: &str = "refs/heads";
const GITBLICK_BRANCH_NAME: &str = "_gitblick";
const GITBLICK_EMAIL: &str = "gitblick@manu.software";

#[derive(Error, Debug)]
pub enum Error {
    #[error("git2 library error")]
    Git2Error(#[from] git2::Error),
    #[error("could not find reference {0}")]
    UnknownReference(String),
    #[error("could not find object {0}")]
    UnknownObject(Oid),
    #[error("uncategorized error")]
    UnknownError(#[from] anyhow::Error),
}

pub struct Repo {
    pub repository: Repository,
    remote_name: String,
    original_branch: Option<String>,
    credentials: CredentialsResolver,
}

impl Clone for Repo {
    fn clone(&self) -> Self {
        Repo::open(
            self.repository.path().parent().unwrap(),
            self.remote_name.clone(),
            self.credentials.clone(),
        )
        .unwrap()
    }
}

impl Repo {
    pub fn open(
        path: &Path,
        remote_name: String,
        credentials: CredentialsResolver,
    ) -> Result<Self, Error> {
        debug!("Opened repository from {path:?}");
        Ok(Repo {
            repository: Repository::open(path)?,
            remote_name,
            original_branch: None,
            credentials,
        })
    }

    pub fn resolve_name_or_get_head(&self, reference: &Option<String>) -> Result<Oid, Error> {
        if let Some(reference_name) = reference {
            if let Ok(oid) = Oid::from_str(reference_name) {
                if self
                    .repository
                    .find_object(oid, Some(git2::ObjectType::Commit))
                    .is_err()
                {
                    return Err(Error::UnknownObject(oid));
                }
                debug!("Resolved target to custom commit {oid:.6?}");
                return Ok(oid);
            }
            if let Ok(git_ref) = self.repository.find_reference(reference_name) {
                let oid = git_ref.peel_to_commit()?.id();
                debug!("Resolved target to reference {oid:.6?}");
                return Ok(oid);
            }
            Err(Error::UnknownReference(reference_name.into()))
        } else {
            let head = self.repository.head()?.peel_to_commit()?.id();
            debug!("Resolved target to HEAD {head:.6}");
            Ok(head)
        }
    }

    fn get_branch_ref(name: &str) -> String {
        if name.starts_with("refs/heads/") {
            name.into()
        } else {
            format!("{}/{}", BASE_HEADS_REF, name)
        }
    }

    fn remote_callbacks(&self) -> RemoteCallbacks {
        let mut cbs = RemoteCallbacks::new();
        cbs.credentials(move |_url, _username_from_url, _allowed_types| {
            self.credentials.get_cred()
        });
        cbs.push_update_reference(|reference, status| {
            match status {
                Some(msg) => warn!(
                    "Could not push reference {reference} to remote, because of status {msg}",
                ),
                None => debug!("Pushed reference {reference} to remote"),
            };

            Ok(())
        });
        cbs.update_tips(|reference, local, remote| {
            debug!("updated reference {reference}\n\tfrom:\t{local:?}\n\tto:\t{remote:?}");
            true
        });
        let mut last_precent: u32 = 0;
        cbs.transfer_progress(move |progress| {
            let cur_precent =
                (progress.received_objects() as f32 / progress.total_objects() as f32 * 10.) as u32;
            if cur_precent != last_precent {
                last_precent = cur_precent;
                debug!(
                    "\ttransfered {}% {}/{} objects",
                    cur_precent * 10,
                    progress.received_objects(),
                    progress.total_objects(),
                );
            }

            true
        });
        cbs
    }

    fn get_signature() -> Result<Signature<'static>, Error> {
        Ok(Signature::now(GITBLICK_EMAIL, GITBLICK_EMAIL)?)
    }

    pub fn find_remote_branch(&self, branch_name: &str) -> Result<(Branch, String), Error> {
        let remote_branch_name = format!("{}/{}", self.remote_name, branch_name);
        Ok((
            self.repository
                .find_branch(&remote_branch_name, BranchType::Remote)?,
            remote_branch_name,
        ))
    }

    pub fn fetch_work_branch(&mut self) -> Result<(), Error> {
        let mut remote = self.create_authenticated_remote()?;

        let mut fo = FetchOptions::new();
        fo.remote_callbacks(self.remote_callbacks());

        let refspec = format!(
            "+refs/heads/{}:refs/remotes/{}/{}",
            GITBLICK_BRANCH_NAME, self.remote_name, GITBLICK_BRANCH_NAME
        );

        debug!("Starting to fetch and pull refspec {refspec}");
        remote.fetch(&[refspec], Some(&mut fo), None)?;
        let stats = remote.stats();
        info!(
            "Fetched and pulled {}/{} objects in {} bytes",
            stats.indexed_objects(),
            stats.total_objects(),
            stats.received_bytes()
        );

        debug!("Found branches:");
        for (branch, branch_type) in (self.repository.branches(None)?).flatten() {
            debug!(
                "\t{:.6?} [{:?}]: {}",
                branch.get().peel_to_commit().unwrap().id(),
                branch_type,
                branch.name().unwrap().unwrap(),
            );
        }
        let (remote_branch, remote_branch_name) = self.find_remote_branch(GITBLICK_BRANCH_NAME)?;
        let remote_commit = remote_branch.get().peel_to_commit()?;
        match self
            .repository
            .find_branch(GITBLICK_BRANCH_NAME, BranchType::Local)
        {
            Ok(mut local_branch) => {
                if local_branch.get().target().unwrap() != remote_branch.get().target().unwrap() {
                    local_branch.get_mut().set_target(remote_commit.id(), "")?;
                    info!(
                        "Updated local gitblick branch reference to point to {:.6?}",
                        remote_commit.id()
                    );
                } else {
                    info!(
                        "Local gitblick branch reference already point to remote commit {:.6?}",
                        remote_commit.id()
                    );
                }
            }
            Err(_) => {
                let mut branch =
                    self.repository
                        .branch(GITBLICK_BRANCH_NAME, &remote_commit, false)?;

                branch.set_upstream(Some(&remote_branch_name))?;

                info!(
                    "Created new local branch pointing to {:.6?}",
                    remote_commit.id()
                )
            }
        };

        Ok(())
    }

    pub fn checkout(&mut self, branch: &str, branch_type: BranchType) -> Result<(), Error> {
        let previous_head = self.repository.head()?.target().unwrap();
        let target_branch = self.repository.find_branch(branch, branch_type)?;
        let target_reference = target_branch.get();
        let mut cob = CheckoutBuilder::new();
        cob.force();
        self.repository.set_head(target_reference.name().unwrap())?;
        self.repository.checkout_head(Some(&mut cob))?;
        info!(
            "Checked out working tree from commit {previous_head:.6?} to branch {} at commit {:.6?}",
            target_reference.name().unwrap(), self.repository.head()?.target().unwrap(),
        );

        Ok(())
    }

    pub fn checkout_work_branch(&mut self) -> Result<(), Error> {
        self.store_original_branch()?;

        self.checkout(GITBLICK_BRANCH_NAME, BranchType::Local)?;

        Ok(())
    }

    fn store_original_branch(&mut self) -> Result<(), Error> {
        let head = self.repository.head()?;
        if head.is_branch() {
            self.original_branch = head.name().map(|value| {
                let path = PathBuf::from_str(value).unwrap();
                path.components()
                    .last()
                    .unwrap()
                    .as_os_str()
                    .to_str()
                    .unwrap()
                    .to_owned()
            });
        }
        if let Some(original_branch) = &self.original_branch {
            info!("Resolved original branch to name {original_branch}");
        } else {
            warn!("Could not determine original branch, will not save and restore later ...")
        };
        Ok(())
    }

    pub fn try_checkout_original_branch(&mut self) -> Result<(), Error> {
        if let Some(original_branch) = self.original_branch.clone() {
            self.checkout(&original_branch, BranchType::Local)?;
        } else {
            info!("Can't checkout original branch as it couldn't be determined before, will do nothing ...");
        }

        Ok(())
    }

    pub fn add_files(&mut self, changed_files: &ChangedFiles) -> Result<Oid, Error> {
        let mut index = self.repository.index()?;
        for file_path in changed_files.iter_relative() {
            index.add_path(file_path)?;
            debug!("Added changed {file_path:?} to index");
        }
        let tree = index.write_tree()?;

        info!("Wrote tree {tree:.6?} to disk");

        Ok(tree)
    }

    pub fn commit_changes(&mut self, tree_oid: Oid) -> Result<Option<Oid>, Error> {
        let parent = self.repository.head()?.peel_to_commit()?;
        if parent.tree_id() == tree_oid {
            warn!("Not creating commit, as no files changed");
            return Ok(None);
        }
        let tree = self.repository.find_tree(tree_oid)?;
        let signature = Repo::get_signature()?;
        let commit = self.repository.commit(
            Some(&Repo::get_branch_ref(GITBLICK_BRANCH_NAME)),
            &signature,
            &signature,
            "",
            &tree,
            &[&parent],
        )?;
        info!("Created commit {commit:?}");

        Ok(Some(commit))
    }

    fn create_authenticated_remote(&self) -> Result<Remote, Error> {
        let original_remote = self.repository.find_remote(&self.remote_name)?;
        let adjusted_url = self
            .credentials
            .transform_remote_url(original_remote.url().unwrap())?;
        let remote = self.repository.remote_anonymous(&adjusted_url)?;
        Ok(remote)
    }

    pub fn push(&mut self) -> Result<(), Error> {
        let mut remote = self.create_authenticated_remote()?;
        let cbs = self.remote_callbacks();

        let mut push_options = PushOptions::new();
        push_options.remote_callbacks(cbs);

        let refspec = Repo::get_branch_ref(GITBLICK_BRANCH_NAME);
        remote.push(&[&refspec], Some(&mut push_options))?;
        info!("Successfully pushed {refspec}");

        Ok(())
    }

    pub fn commit_and_push_changed_files(
        &mut self,
        changed_files: &ChangedFiles,
    ) -> anyhow::Result<()> {
        let tree_oid = self
            .add_files(changed_files)
            .with_context(|| format!("Couldn't add changed files {changed_files:?}"))?;
        if self
            .commit_changes(tree_oid)
            .context("Couldn't create commit")?
            .is_some()
        {
            self.push().context("Couldn't push commit")?;
        }
        Ok(())
    }
}
