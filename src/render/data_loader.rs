use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

use anyhow::anyhow;
use git2::Oid;

use crate::{
    git::repo::Repo,
    record::{PointValue, Record, RecordMarshal},
};

struct Records {
    record_marshal: RecordMarshal,
    records: HashMap<Oid, Record>,
}

#[allow(dead_code)]
impl Records {
    pub fn new(record_marshal: RecordMarshal) -> Self {
        Self {
            record_marshal,
            records: HashMap::new(),
        }
    }

    fn get_record(&mut self, oid: &Oid) -> anyhow::Result<Record> {
        if let Some(record) = self.records.get(oid) {
            return Ok(record.clone());
        }

        let val = self.record_marshal.load(*oid)?;
        self.records.insert(*oid, val.clone());
        Ok(val)
    }

    fn get_record_point(&mut self, oid: &Oid, name: &str) -> anyhow::Result<PointValue> {
        let record = self.get_record(oid)?;
        match record.points.get(name) {
            None => Err(anyhow!(
                "Record for commit {:.6} exists, but not the point {name}",
                oid
            )),
            Some(point_value) => Ok(point_value.clone()),
        }
    }

    fn get_string_value(&mut self, oid: &Oid, name: &str) -> anyhow::Result<String> {
        match self.get_record_point(oid, name)? {
            PointValue::String(val) => Ok(val),
            _ => Err(anyhow!(
                "Point {name} for record {:?} is not of type String!",
                oid
            )),
        }
    }

    fn get_integer_value(&mut self, oid: &Oid, name: &str) -> anyhow::Result<i64> {
        match self.get_record_point(oid, name)? {
            PointValue::Integer(val) => Ok(val),
            _ => Err(anyhow!(
                "Point {name} for record {:?} is not of type Integer!",
                oid
            )),
        }
    }
}

pub struct DataLoader {
    records: Records,
    repo: Repo,
}

impl DataLoader {
    pub fn new(record_marshal: RecordMarshal, repo: Repo) -> Self {
        Self {
            records: Records::new(record_marshal),
            repo,
        }
    }

    pub fn get_latest_commit(&mut self, branch_name: &str) -> anyhow::Result<Oid> {
        let (branch, _) = self.repo.find_remote_branch(branch_name)?;
        Ok(branch.get().peel_to_commit().unwrap().id())
    }

    pub fn get_latest_value(
        &mut self,
        point_name: &str,
        branch_name: &str,
    ) -> anyhow::Result<String> {
        let last_commit = self.get_latest_commit(branch_name)?;
        let point = self.records.get_record_point(&last_commit, point_name)?;

        Ok(format!("{}", point))
    }
}

#[derive(Clone)]
pub struct DataLoaderProxy {
    data_loader: Arc<Mutex<DataLoader>>,
}

impl From<DataLoader> for DataLoaderProxy {
    fn from(data_loader: DataLoader) -> Self {
        Self {
            data_loader: Arc::new(Mutex::new(data_loader)),
        }
    }
}

impl DataLoaderProxy {
    pub fn get_latest_commit(&self, branch_name: &str) -> anyhow::Result<Oid> {
        self.data_loader
            .lock()
            .unwrap()
            .get_latest_commit(branch_name)
    }

    pub fn get_latest_value(&self, point_name: &str, branch_name: &str) -> anyhow::Result<String> {
        self.data_loader
            .lock()
            .unwrap()
            .get_latest_value(point_name, branch_name)
    }
}
