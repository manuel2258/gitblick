use chrono::Utc;
use serde::{Deserialize, Serialize};

use crate::config::ProjectConfig;

#[derive(Serialize, Deserialize)]
pub struct MetaData {
    current_time: String,
    source_commit: String,
}

impl MetaData {
    pub fn new(source_commit: String) -> Self {
        Self {
            current_time: Utc::now().to_rfc3339(),
            source_commit,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct RenderData {
    pub project: ProjectConfig,
    pub meta: MetaData,
}

impl From<RenderData> for tera::Context {
    fn from(val: RenderData) -> Self {
        let value = serde_json::to_value(val).unwrap();
        tera::Context::from_value(value).unwrap()
    }
}
