use std::{
    collections::HashMap,
    fs::{self, OpenOptions},
    path::{Path, PathBuf},
};

use anyhow::Context;
use log::debug;
use tera::{Tera, Value};

use crate::{changed_files::ChangedFiles, config::TeraOutput, render::data::RenderData};

use super::data_loader::{DataLoader, DataLoaderProxy};

pub struct TeraRenderer {
    tera: Tera,
    context: tera::Context,
    config: TeraOutput,
    repo_path: PathBuf,
}

impl TeraRenderer {
    pub fn new(
        data: RenderData,
        data_loader: DataLoader,
        output_config: TeraOutput,
        repo_path: &Path,
    ) -> anyhow::Result<Self> {
        let source_path = repo_path.join(&output_config.source);
        debug!("Creating tera engine with templates {source_path:?}");
        let mut tera = Tera::new(source_path.as_os_str().to_str().unwrap())?;
        TeraRenderer::register_functions(&mut tera, data_loader.into());
        Ok(TeraRenderer {
            tera,
            context: data.into(),
            config: output_config,
            repo_path: repo_path.into(),
        })
    }

    pub fn render_all(&mut self, changed_files: &mut ChangedFiles) -> anyhow::Result<()> {
        let templates: Vec<String> = self
            .tera
            .get_template_names()
            .map(|val| val.to_owned())
            .collect();
        for template in templates {
            self.render(&template, changed_files)?;
        }

        Ok(())
    }

    fn render(&mut self, name: &str, changed_files: &mut ChangedFiles) -> anyhow::Result<()> {
        let destination_path = self.repo_path.join(&self.config.destination).join(name);
        fs::create_dir_all(destination_path.parent().unwrap())?;
        let destination = OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(&destination_path)
            .with_context(|| format!("Couldn't open file {destination_path:?}"))?;
        self.tera.render_to(name, &self.context, destination)?;
        debug!("Rendered {name} to {destination_path:?}");
        changed_files.add(destination_path);
        Ok(())
    }

    fn register_functions(tera: &mut Tera, data_loader: DataLoaderProxy) {
        tera.register_filter(
            "get_latest_commit",
            move |value: &Value, _: &HashMap<String, Value>| match value {
                Value::String(branch_name) => match data_loader.get_latest_commit(branch_name) {
                    Ok(commit) => Ok(Value::String(commit.to_string())),
                    Err(err) => Err(tera::Error::msg(err.to_string())),
                },
                _ => Err(tera::Error::msg("only string arguments are valid!")),
            },
        );
    }
}
