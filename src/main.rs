use anyhow::Context;
use changed_files::ChangedFiles;
use clap::Parser;
use log::info;
use record::RecordMarshal;
use render::{data_loader::DataLoader, tera::TeraRenderer};

use crate::context::AppContext;

pub mod changed_files;
pub mod cli;
pub mod config;
pub mod context;
pub mod git;
pub mod record;
pub mod render;

fn main() {
    pretty_env_logger::init();
    let cli = cli::Cli::parse();

    let res = match cli.command.clone() {
        Some(cli::Commands::Append { .. }) => append(cli),
        Some(cli::Commands::List { .. }) => list(cli),
        Some(cli::Commands::RenderAll) => render_all(cli),
        Some(cli::Commands::Checkout) => checkout(cli),
        None => Ok(()),
    };

    match res {
        Ok(..) => info!("Completed without any issues"),
        Err(err) => panic!("Error while execution:\n{err:?}"),
    }
}

fn append(cli: cli::Cli) -> anyhow::Result<()> {
    if let Some(cli::Commands::Append { name, value, .. }) = cli.command.clone() {
        let mut ctx = AppContext::open_and_load_from_cli(cli)?;

        let record_marshal = RecordMarshal::new(
            ctx.cli.get_repo_path().join("records"),
            ctx.config.points.clone(),
        );
        let mut changed_files = ChangedFiles::new(ctx.cli.get_repo_path());

        record_marshal
            .write_new_point(
                ctx.target_commit,
                name.clone(),
                value.clone().into_bytes(),
                &mut changed_files,
            )
            .with_context(|| {
                format!(
                    "Could not write value {} for point {} on commit {}",
                    value, name, ctx.target_commit
                )
            })?;
        let record = record_marshal.load(ctx.target_commit).with_context(|| {
            format!("Could not load all points on commit {}", ctx.target_commit)
        })?;
        println!("{}", record);

        ctx.repo.commit_and_push_changed_files(&changed_files)?;

        ctx.repo.try_checkout_original_branch()?;
    }

    Ok(())
}

fn list(cli: cli::Cli) -> anyhow::Result<()> {
    if let Some(cli::Commands::List { .. }) = cli.command.clone() {
        let mut ctx = AppContext::open_and_load_from_cli(cli)?;

        let record_marshal = RecordMarshal::new(
            ctx.cli.get_repo_path().join("records"),
            ctx.config.points.clone(),
        );

        let record = record_marshal.load(ctx.target_commit)?;
        println!("{}", record);

        ctx.repo.try_checkout_original_branch()?;
    }

    Ok(())
}

fn render_all(cli: cli::Cli) -> anyhow::Result<()> {
    let mut ctx = AppContext::open_and_load_from_cli(cli)?;

    let mut changed_files = ChangedFiles::new(ctx.cli.get_repo_path());

    for output in &ctx.config.outputs {
        match output {
            config::Output::Tera(output_config) => {
                let record_marshal = RecordMarshal::new(
                    ctx.cli.get_repo_path().join("records"),
                    ctx.config.points.clone(),
                );
                let mut tera_renderer = TeraRenderer::new(
                    render::data::RenderData {
                        project: ctx.config.project.clone(),
                        meta: ctx.get_meta_data()?,
                    },
                    DataLoader::new(record_marshal, ctx.repo.clone()),
                    output_config.clone(),
                    &ctx.cli.get_repo_path(),
                )?;
                tera_renderer
                    .render_all(&mut changed_files)
                    .with_context(|| {
                        format!("Couldn't render the tera output {output_config:?}")
                    })?;
            }
        }
    }

    if !ctx.cli.dry_run {
        ctx.repo.commit_and_push_changed_files(&changed_files)?;
        ctx.repo.try_checkout_original_branch()?;
    }

    Ok(())
}

fn checkout(cli: cli::Cli) -> anyhow::Result<()> {
    AppContext::open_and_load_from_cli(cli)?;
    Ok(())
}
