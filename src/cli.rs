use std::path::PathBuf;

use clap::{Args, Parser, Subcommand};

#[derive(Parser)]
#[command(version, about, long_about = None)]
pub struct Cli {
    #[arg(short, long)]
    pub config: Option<PathBuf>,

    #[arg(short, long)]
    pub repo: Option<PathBuf>,

    #[arg(long)]
    pub remote: Option<String>,

    #[arg(short, long, default_value_t = false)]
    pub dry_run: bool,

    #[command(flatten)]
    pub authentication: Authentication,

    #[command(subcommand)]
    pub command: Option<Commands>,
}

impl Cli {
    pub fn get_config_path(&self) -> PathBuf {
        match &self.config {
            Some(val) => val.clone(),
            None => self.get_repo_path().join("gitblick.ron"),
        }
    }

    pub fn get_repo_path(&self) -> PathBuf {
        match &self.repo {
            Some(val) => val.clone(),
            None => ".".into(),
        }
    }

    pub fn get_remote(&self) -> String {
        match &self.remote {
            Some(val) => val.clone(),
            None => "origin".into(),
        }
    }
}

#[derive(Args, Clone)]
#[group(multiple(true))]
pub struct Authentication {
    #[arg(long, default_value = "git")]
    pub auth_username: String,
    #[arg(long)]
    pub http_password: Option<String>,

    #[arg(long)]
    pub ssh_private_key: Option<PathBuf>,
    #[arg(long)]
    pub ssh_passphrase: Option<String>,
}

#[derive(Subcommand, Clone)]
pub enum Commands {
    Append {
        #[arg(short, long)]
        commit: Option<String>,
        #[arg(short, long)]
        name: String,
        #[arg(short, long)]
        value: String,
    },
    List {
        #[arg(short, long)]
        commit: Option<String>,
    },
    RenderAll,
    Checkout,
}
