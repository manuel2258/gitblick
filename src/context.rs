use crate::{
    cli::{self, Cli},
    config::Config,
    git::repo::Repo,
    render::data::MetaData,
};
use anyhow::Context;
use git2::Oid;
use log::info;

pub struct AppContext {
    pub cli: Cli,
    pub config: Config,
    pub repo: Repo,
    pub target_commit: Oid,
}

impl AppContext {
    pub fn open_and_load_from_cli(cli: Cli) -> Result<AppContext, anyhow::Error> {
        let mut repo = Repo::open(
            &cli.get_repo_path(),
            cli.get_remote(),
            cli.authentication.clone().into(),
        )
        .with_context(|| format!("Could not open the git repo at {:?}", &cli.get_repo_path(),))?;
        let specified_head = match &cli.command {
            Some(cli::Commands::Append { commit, .. }) => commit.clone(),
            Some(cli::Commands::List { commit, .. }) => commit.clone(),
            Some(cli::Commands::RenderAll) => None,
            Some(cli::Commands::Checkout) => None,
            None => None,
        };
        let target_commit = repo.resolve_name_or_get_head(&specified_head)?;
        if !cli.dry_run {
            repo.fetch_work_branch()
                .with_context(|| "Could not fetch the git gitblick working branch".to_string())?;

            repo.checkout_work_branch().with_context(|| {
                "Could not checkout the git gitblick working branch".to_string()
            })?;
        } else {
            info!("Skip fetching and checking out gitblick branch, as it is a dry run!");
        }

        let config = Config::load_from_file(&cli.get_config_path()).with_context(|| {
            format!(
                "Could not load the config file from {:?}",
                &cli.get_config_path(),
            )
        })?;

        Ok(AppContext {
            cli,
            config,
            repo,
            target_commit,
        })
    }

    pub fn get_meta_data(&self) -> anyhow::Result<MetaData> {
        let head = self.repo.resolve_name_or_get_head(&None)?;
        Ok(MetaData::new(head.to_string()))
    }
}
