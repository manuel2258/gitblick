use anyhow::Context;
use log::{info, warn};
use std::{
    collections::HashMap,
    fmt::Display,
    fs::{self},
    io,
    path::{Path, PathBuf},
};

use thiserror::Error;

use git2::Oid;

use crate::{
    changed_files::ChangedFiles,
    config::{PointDefinitionType, PointDefinitions},
};

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum PointValue {
    Integer(i64),
    String(String),
    Bytes(Vec<u8>),
}

impl Display for PointValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PointValue::Integer(val) => write!(f, "{}", val),
            PointValue::String(val) => write!(f, "{}", val),
            PointValue::Bytes(val) => write!(f, "{:?}", val),
        }
    }
}

impl PointValue {
    pub fn from_data(
        data: Vec<u8>,
        point_type: PointDefinitionType,
    ) -> Result<Self, anyhow::Error> {
        Ok(match point_type {
            PointDefinitionType::Integer => PointValue::Integer(
                String::from_utf8(data)?
                    .parse()
                    .context("Can't convert value to integer")?,
            ),
            PointDefinitionType::String => PointValue::String(String::from_utf8(data)?),
            PointDefinitionType::Bytes => PointValue::Bytes(data),
        })
    }

    pub fn to_data(&self) -> Vec<u8> {
        match self {
            PointValue::Integer(val) => val.to_string().into_bytes(),
            PointValue::String(val) => val.clone().into_bytes(),
            PointValue::Bytes(val) => val.clone(),
        }
    }

    pub fn from_file(path: &Path, point_type: PointDefinitionType) -> Result<Self, anyhow::Error> {
        let data = fs::read(path)?;
        Self::from_data(data, point_type)
    }

    pub fn to_file(&self, path: &Path) -> Result<(), anyhow::Error> {
        fs::write(path, self.to_data())?;
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Record {
    pub commit: Oid,
    pub points: HashMap<String, PointValue>,
}

impl Display for Record {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Records for commit {}:", self.commit)?;
        for (name, point_data) in &self.points {
            write!(f, "\n\t - {}: {:?}", name, point_data)?;
        }
        Ok(())
    }
}

pub struct RecordMarshal {
    records_directory: PathBuf,
    points: PointDefinitions,
}

#[derive(Error, Debug)]
pub enum MarshalError {
    #[error("Record for commit {0} does not exist")]
    RecordNotExisting(Oid),
    #[error("Record directory for commit {0} at {1} is a file and not a directory")]
    RecordDirectoryIsFile(Oid, PathBuf),
    #[error("Point file {0} for commit {1} at {2} is a directory and not a file")]
    PointFileIsDirectory(String, Oid, PathBuf),
    #[error("Unknown io error: {0}")]
    UnknownIO(#[from] io::Error),
    #[error("Point name {0} is not defined in the configuration")]
    PointNotConfigured(String),
}

impl RecordMarshal {
    pub fn new(records_directory: PathBuf, points: HashMap<String, PointDefinitionType>) -> Self {
        Self {
            records_directory,
            points,
        }
    }

    fn get_and_ensure_record_directory(
        &self,
        commit: Oid,
        create_if_not_exist: bool,
    ) -> Result<PathBuf, anyhow::Error> {
        let record_directory = self.records_directory.join(commit.to_string());
        if !record_directory.exists() {
            if create_if_not_exist {
                info!(
                    "Record directory '{:?}' does not exist, creating it",
                    record_directory
                );
                fs::create_dir_all(&record_directory)?;
                return Ok(record_directory);
            }
            return Err(MarshalError::RecordNotExisting(commit).into());
        }
        if record_directory.is_file() {
            return Err(MarshalError::RecordDirectoryIsFile(commit, record_directory).into());
        }
        Ok(record_directory)
    }

    pub fn load(&self, commit: Oid) -> Result<Record, anyhow::Error> {
        let record_directory = self.get_and_ensure_record_directory(commit, false)?;

        let mut record = Record {
            commit,
            points: HashMap::new(),
        };

        for sub_entry in record_directory.read_dir()? {
            let sub_file = sub_entry?;
            let point_name = sub_file.file_name().to_str().unwrap().into();
            if !sub_file.file_type()?.is_file() {
                return Err(MarshalError::PointFileIsDirectory(
                    point_name,
                    commit,
                    sub_file.path(),
                )
                .into());
            }
            let point_type = self.get_point_type(&point_name)?;

            record.points.insert(
                point_name,
                PointValue::from_file(&sub_file.path(), point_type)?,
            );
        }

        Ok(record)
    }

    fn get_point_type(&self, point_name: &String) -> Result<PointDefinitionType, anyhow::Error> {
        let point_type = self
            .points
            .get(point_name)
            .ok_or(MarshalError::PointNotConfigured(point_name.clone()))?;
        Ok(point_type.to_owned())
    }

    pub fn write(
        &self,
        record: &Record,
        changed_files: &mut ChangedFiles,
    ) -> Result<(), anyhow::Error> {
        let record_directory = self.get_and_ensure_record_directory(record.commit, true)?;

        for (point_name, point_value) in record.points.iter() {
            let point_type = self.get_point_type(point_name)?;
            let point_file = record_directory.join(point_name);
            let old_point_value_opt = if point_file.exists() {
                Some(PointValue::from_file(&point_file, point_type)?)
            } else {
                None
            };

            if let Some(old_point_value) = &old_point_value_opt {
                if point_value == old_point_value {
                    info!(
                        "Skipped writting point {} with data {:?}, as it already exists with the same data",
                        point_name, point_value
                    );
                    continue;
                }
            }

            point_value.to_file(&point_file)?;
            changed_files.add(point_file);

            if let Some(old_data) = old_point_value_opt {
                warn!(
                    "Successfully overwrote existing point {} old data {:?} with new data {:?}",
                    point_name, old_data, point_value
                );
            } else {
                info!(
                    "Successfully wrote new point {} with data {:?}",
                    point_name, point_value
                );
            }
        }

        Ok(())
    }

    pub fn write_new_point(
        &self,
        commit: Oid,
        name: String,
        data: Vec<u8>,
        changed_files: &mut ChangedFiles,
    ) -> Result<(), anyhow::Error> {
        let point_type = self.get_point_type(&name)?;
        let mut points = HashMap::new();
        points.insert(name, PointValue::from_data(data, point_type)?);
        let record = Record { commit, points };

        self.write(&record, changed_files)?;
        Ok(())
    }
}
