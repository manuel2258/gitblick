use std::{
    collections::HashMap,
    fs, io,
    path::{Path, PathBuf},
};

use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("unknown io error: {0}")]
    UnknownIO(#[from] io::Error),
    #[error("toml deserialization error: {0}")]
    TomlDeserialization(#[from] ron::error::SpannedError),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TeraOutput {
    pub source: String,
    pub destination: PathBuf,
}

#[derive(Serialize, Deserialize, Clone)]
pub enum Output {
    Tera(TeraOutput),
}

#[derive(Serialize, Deserialize, Clone, Copy)]
pub enum PointDefinitionType {
    Integer,
    String,
    Bytes,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ProjectConfig {
    pub name: String,
}

pub type PointDefinitions = HashMap<String, PointDefinitionType>;

#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    pub project: ProjectConfig,

    pub points: PointDefinitions,
    pub outputs: Vec<Output>,
}

impl Config {
    pub fn load_from_file(file: &Path) -> Result<Self, Error> {
        let config_content = fs::read_to_string(file)?;
        Ok(ron::from_str(&config_content)?)
    }
}
