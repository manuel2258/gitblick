{
  description = "A git metrics tracking and visualization tool";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.rust-analyzer-src.follows = "";
    };

    flake-utils.url = "github:numtide/flake-utils";

    advisory-db = {
      url = "github:rustsec/advisory-db";
      flake = false;
    };

  };

  outputs = { self, nixpkgs, crane, fenix, flake-utils, advisory-db, ... }:
    flake-utils.lib.eachSystem ["x86_64-linux"] (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        inherit (pkgs) lib;

        craneLib = crane.lib.${system};
        gitblick-src = craneLib.cleanCargoSource (craneLib.path ./.);

        commonArgs = with pkgs; {
          src = gitblick-src;
          strictDeps = true;

          nativeBuildInputs = lib.optionals stdenv.isLinux [ pkg-config ];
          buildInputs = lib.optionals stdenv.isLinux [ openssl ];

          # Additional environment variables can be set directly
          # MY_CUSTOM_VAR = "some value";
        };

        rust-tools = fenix.packages.${system}.complete.withComponents [
          "cargo"
          "rustc"
        ];

        craneLibLLvmTools = craneLib.overrideToolchain (rust-tools);
        gitblick-crate-deps = craneLib.buildDepsOnly commonArgs;

        gitblick-crate = craneLib.buildPackage (commonArgs // {
          cargoArtifacts = gitblick-crate-deps;
        });

        gitblick-ci-image = pkgs.dockerTools.buildImage {
          name = "registry.gitlab.com/manuel2258/gitblick/gitblick-ci";
          tag = "latest";
          created = "now";
          # enableFakechroot = true;
          runAsRoot = ''
            mkdir -p /tmp
            chmod 1777 /tmp

            ${pkgs.dockerTools.shadowSetup}

            echo "nixbld:x:999:" > /etc/group
            echo "root:x:0:" >> /etc/group

            for n in $(seq 1 10); do
              useradd -c "Nix build user $n" \
                -d /var/empty \
                -g nixbld \
                -G nixbld \
                -M \
                -N \
                -r \
                -s "$(command -v nologin)" \
                "nixbld$n"
            done
          '';
          copyToRoot = pkgs.buildEnv {
            name = "image-root";
            pathsToLink = [ "/bin" "/etc/nix" "/etc/ssl" ];
            paths = [
              (pkgs.writeTextFile {
                name = "nix.conf";
                destination = "/etc/nix/nix.conf";
                text = ''
                  accept-flake-config = true
                  experimental-features = nix-command flakes
                '';
              })

              pkgs.bash
              pkgs.gnugrep
              pkgs.coreutils
              pkgs.nix
              pkgs.cacert
              pkgs.gitMinimal
              pkgs.crane
              pkgs.gzip
              pkgs.cachix
            ];
          };

          config = {
            Cmd = [ "/bin/bash" ];
            Env = [
              "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
            ];
          };
        };

        gitblick-image = pkgs.dockerTools.buildLayeredImage {
          name = "registry.gitlab.com/manuel2258/gitblick/gitblick";
          tag = "latest";
          created = "now";
          contents = [
            gitblick-crate
            pkgs.bash
            pkgs.coreutils
            pkgs.cacert
            pkgs.gnugrep
            pkgs.gitMinimal
          ];

          config = {
            Env = [
              "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
            ];
          };
        };

      in
      {
        checks = {
          inherit gitblick-crate;

          gitblick-crate-clippy = craneLib.cargoClippy (commonArgs // {
            cargoArtifacts = gitblick-crate-deps;
            cargoClippyExtraArgs = "--all-targets -- --deny warnings";
          });

          gitblick-crate-doc = craneLib.cargoDoc (commonArgs // {
            cargoArtifacts = gitblick-crate-deps;
          });

          gitblick-crate-fmt = craneLib.cargoFmt {
            src = gitblick-src;
          };

          gitblick-crate-audit = craneLib.cargoAudit {
            inherit  advisory-db;
            src = gitblick-src;
          };

          # gitblick-crate-deny = craneLib.cargoDeny {
          #   inherit src;
          # };

          # Consider setting `doCheck = false` on `gitblick-crate` if you do not want
          # the tests to run twice
          # gitblick-crate-nextest = craneLib.cargoNextest (commonArgs // {
          #   inherit cargoArtifacts;
          #   partitions = 1;
          #   partitionType = "count";
          # });

        };

        packages = {
          default = gitblick-crate;
          inherit gitblick-image;
          inherit gitblick-ci-image;
        } // lib.optionalAttrs (!pkgs.stdenv.isDarwin) {
          gitblick-crate-llvm-coverage = craneLibLLvmTools.cargoLlvmCov (commonArgs // {
            cargoArtifacts = gitblick-crate-deps;
          });
        };

        apps.default = flake-utils.lib.mkApp {
          drv = gitblick-crate;
        };

        devShells.default = craneLib.devShell {
          checks = self.checks.${system};

          # Additional dev-shell environment variables can be set directly
          # MY_CUSTOM_DEVELOPMENT_VAR = "something else";

          packages = [
            pkgs.rust-analyzer
            pkgs.pre-commit
          ];
        };
      });
}