FROM rust:alpine as rust_builder

RUN apk add --no-cache gcc musl-dev openssl-dev

RUN cargo install sccache

WORKDIR /work

COPY src src
COPY Cargo.toml Cargo.toml
COPY Cargo.lock Cargo.lock

RUN cargo build --release

FROM alpine:edge

COPY --from=rust_builder /work/target/release/gitblick /usr/local/bin/gitblick

